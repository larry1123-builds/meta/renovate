# syntax = docker/dockerfile:experimental
FROM mhart/alpine-node:12@sha256:5786e9ce414c01502aa760ad44f76d0e3da09b5629dda176573d959faffcceb2 as install-prod
WORKDIR /opt/renovate

RUN --mount=type=cache,sharing=locked,target=/var/cache/apk/ apk add --update \
        python \
        make \
        g++ \
    ;

COPY package.json .
COPY yarn.lock .
COPY .yarnclean .

RUN --mount=type=cache,sharing=locked,target=/usr/local/share/.cache/yarn/ \
  yarn install --production=true --frozen-lockfile

FROM mhart/alpine-node:12@sha256:5786e9ce414c01502aa760ad44f76d0e3da09b5629dda176573d959faffcceb2 as final
WORKDIR /opt/renovate

RUN --mount=type=cache,sharing=locked,target=/var/cache/apk/ apk add --update \
        git \
        bash \
    ;

COPY --from=install-prod /opt/renovate/node_modules node_modules
